<?php

namespace App\Observers;

use App\Jobs\IndexProductElasticsearchJob;
use App\Jobs\UpdateProductElasticsearchJob;
use App\Jobs\RemoveProductElasticsearchJob;
use App\Models\Product;

class ProductObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        dispatch(new IndexProductElasticsearchJob($product));
    }

    /**
     * Handle the Product "updated" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        dispatch(new UpdateProductElasticsearchJob($product));
    }

    /**
     * Handle the Product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        dispatch(new RemoveProductElasticsearchJob($product->id));
    }

    /**
     * Handle the Product "restored" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        dispatch(new IndexProductElasticsearchJob($product));
    }

    /**
     * Handle the Product "force deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        dispatch(new RemoveProductElasticsearchJob($product->id));
    }
}
