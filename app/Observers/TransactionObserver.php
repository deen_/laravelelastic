<?php

namespace App\Observers;

use App\Jobs\UpdateTransactionElasticsearchJob;
use App\Jobs\RemoveTransactionElasticsearchJob;
use App\Models\Transaction;

class TransactionObserver
{
    /**
     * Handle the Transaction "updated" event.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return void
     */
    public function updated(Transaction $transaction)
    {
        dispatch(new UpdateTransactionElasticsearchJob($transaction));
    }
}
