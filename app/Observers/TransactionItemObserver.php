<?php

namespace App\Observers;

use App\Jobs\IndexTransactionItemElasticsearchJob;
use App\Jobs\UpdateTransactionItemElasticsearchJob;
use App\Jobs\RemoveTransactionItemElasticsearchJob;
use App\Models\TransactionItem;

class TransactionItemObserver
{
    /**
     * Handle the TransactionItem "created" event.
     *
     * @param  \App\Models\TransactionItem  $transactionItem
     * @return void
     */
    public function created(TransactionItem $transactionItem)
    {
        dispatch(new IndexTransactionItemElasticsearchJob($transactionItem));
    }

    /**
     * Handle the TransactionItem "updated" event.
     *
     * @param  \App\Models\TransactionItem  $transactionItem
     * @return void
     */
    public function updated(TransactionItem $transactionItem)
    {
        dispatch(new UpdateTransactionItemElasticsearchJob($transactionItem));
    }

    /**
     * Handle the TransactionItem "deleted" event.
     *
     * @param  \App\Models\TransactionItem  $transactionItem
     * @return void
     */
    public function deleted(TransactionItem $transactionItem)
    {
        dispatch(new RemoveTransactionItemElasticsearchJob($transactionItem->id));
    }

    /**
     * Handle the TransactionItem "restored" event.
     *
     * @param  \App\Models\TransactionItem  $transactionItem
     * @return void
     */
    public function restored(TransactionItem $transactionItem)
    {
        dispatch(new IndexTransactionItemElasticsearchJob($transactionItem));
    }

    /**
     * Handle the TransactionItem "force deleted" event.
     *
     * @param  \App\Models\TransactionItem  $transactionItem
     * @return void
     */
    public function forceDeleted(TransactionItem $transactionItem)
    {
        dispatch(new RemoveTransactionItemElasticsearchJob($transactionItem->id));
    }
}
