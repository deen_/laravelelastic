<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;
use Elasticsearch\Client;

class IndexProductElasticsearchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Client $client)
    {
        $params = [
            'index' => 'products',
            'type' => '_doc',
            'id' => $this->product->id,
            'body' => [
                'stock_code' => $this->product->stock_code,
                'name' => $this->product->name,
                'category' => ($this->product->category ? $this->product->category->name : ""),
                'brand' => ($this->product->brand ? $this->product->brand->name : ""),
                'color' => ($this->product->color ? $this->product->color->name : ""),
                'size' => ($this->product->size ? $this->product->size->name : ""),
                'unit_price' => floatval($this->product->unit_price)
            ],
        ];

        $client->index($params);
    }
}
