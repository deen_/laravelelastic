<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TransactionItem;
use Elasticsearch\Client;

class IndexTransactionItemElasticsearchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transaction_item;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TransactionItem $transaction_item)
    {
        $this->transaction_item = $transaction_item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Client $client)
    {
        $invoice_date = date("m/d/Y H:i", strtotime($this->transaction_item->transaction->invoice_date));
        
        $params = [
            'index' => 'transactions',
            'type' => '_doc',
            'id' => $this->transaction_item->id,
            'body' => [
                'invoice_no' => $this->transaction_item->transaction->invoice_no,
                'stock_code' => $this->transaction_item->product->stock_code,
                'description' => $this->transaction_item->product->name,
                'quantity' => $this->transaction_item->quantity,
                'invoice_date' => $invoice_date,
                'unit_price' => floatval($this->transaction_item->unit_price),
                'category' => $this->transaction_item->product->category->name,
                'customer_id' => $this->transaction_item->transaction->customer_id,
                'customer' => $this->transaction_item->transaction->customer->name
            ],
        ];

        $client->index($params);
    }
}
