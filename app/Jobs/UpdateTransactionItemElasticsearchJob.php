<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Elasticsearch\Client;

class UpdateTransactionItemElasticsearchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transaction_item;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($transaction_item)
    {
        $this->transaction_item = $transaction_item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Client $client)
    {
        $params = [
            'index' => 'transactions',
            'type' => '_doc',
            'id' => $this->transaction_item->id,
            'body' => [
                'doc' => [
                    'stock_code' => $this->transaction_item->product->stock_code,
                    'description' => $this->transaction_item->product->name,
                    'quantity' => $this->transaction_item->quantity,
                    'unit_price' => floatval($this->transaction_item->unit_price),
                ]
            ]
        ];

        try {
            $client->update($params);
        } catch (Missing404Exception $exception) {
            // Already deleted..
        }
    }
}
