<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Transaction;
use Elasticsearch\Client;

class UpdateTransactionElasticsearchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transaction;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Client $client)
    {   
        foreach($this->transaction->items AS $item) {

            $invoice_date = date("m/d/Y H:i", strtotime($this->transaction->invoice_date));
            
            $params['body'][] = [
                'update' => [
                    '_index' => 'transactions',
                    '_type' => '_doc',
                    '_id' => $item->id
                ]
            ];
            $params['body'][] = [
                'doc' => [
                    'invoice_no' => $item->transaction->invoice_no,
                    'stock_code' => $item->product->stock_code,
                    'description' => $item->product->name,
                    'quantity' => $item->quantity,
                    'invoice_date' => $invoice_date,
                    'unit_price' => floatval($item->unit_price),
                    'customer_id' => $item->transaction->customer_id    
                ]
            ];
        }

        try {
            $client->bulk($params);
        } catch (Missing404Exception $exception) {
            // Already deleted..
        }
    }
}
