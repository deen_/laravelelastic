<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ProductReview;
use Elasticsearch\Client;

class IndexProductReviewElasticsearchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productReview;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ProductReview $productReview)
    {
        $this->productReview = $productReview;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Client $client)
    {
        $params = [
            'index' => 'product_reviews',
            'type' => '_doc',
            'id' => $this->productReview->id,
            'body' => [
                'stock_code' => $this->productReview->product->stock_code,
                'category' => $this->productReview->product->category->name,
                'product_name' => $this->productReview->product->name,
                'star' => $this->productReview->star,
                'review' => $this->productReview->review,
                'customer' => ($this->productReview->reviewer ? $this->productReview->reviewer->name : ""),
            ],
        ];

        $client->index($params);
    }
}
