<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    use HasFactory;
    protected $fillable = ['customer_address_id', 'shipment_date', 'shipment_received'];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function address()
    {
        return $this->belongsTo(CustomerAddress::class);
    }
}
