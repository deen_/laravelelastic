<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    use HasFactory;
    protected $fillable = ['full_name', 'barangay_id', 'postal_code', 'detailed_address', 'default_address'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function barangay()
    {
        return $this->belongsTo(Barangay::class);
    }
}
