<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable = ['id', 'gender', 'dob'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function wishlist()
    {
        return $this->hasMany(Wishlist::class);   
    }

    public function addresses()
    {
        return $this->hasMany(CustomerAddress::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }
}
