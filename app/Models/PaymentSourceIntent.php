<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentSourceIntent extends Model
{
    use HasFactory;
    protected $fillable = ['transaction_id', 'payment_intent_id', 'source_id'];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
