<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    use HasFactory;
    
    protected $guarded = [];

    public function category() 
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function transaction_items() 
    {
        return $this->hasMany(TransactionItem::class);
    }

    public function images() 
    {
        return $this->hasMany(ProductImage::class);
    }

    public function reviews() 
    {
        return $this->hasMany(ProductReview::class);
    }
    
    public function cart()
    {
        return $this->hasMany(Cart::class);
    }
}
