<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionConfirmationEmail;

class Transaction extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function items() 
    {
        return $this->hasMany(TransactionItem::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function shipments() 
    {
        return $this->hasMany(Shipment::class);
    }

    public function payments() 
    {
        return $this->hasMany(Payment::class);
    }

    public function payment_source_intents() 
    {
        return $this->hasMany(PaymentSourceIntent::class);
    }

    public function recordPayment($payment)
    {
        $this->payments()->create([
            "amount" => $payment->amount,
            "reference_no" => $payment->id,
            "payment_method" => $payment->method,
        ]);

        $this->updateBalance($payment->amount);
    }

    public function updateBalance($payment)
    {
        $this->balance = $this->balance - $payment;
        if ($this->balance == 0) {
            $this->payment_status = "PAID";
        } else {
            $this->payment_status = "PARTIAL PAYMENT";
        }
        $this->save();
    }

    public function sendConfirmationEmail()
    {
        Mail::to($this->customer->user->email)->send(
            new TransactionConfirmationEmail($this->dataset())
        );
    }

    public function dataset()
    {
        // change this to auto
        $transaction = [
            'id' => $this->id,
            'sub_total' => $this->sub_total,
            'shipping_amount' => $this->shipping_amount,
            'discount_amount' => $this->discount_amount,
            'total_amount' => $this->total_amount,
            'balance' => $this->balance,
            'payment_status' => $this->payment_status
        ];

        $data = [
            'message' => 'This is a test  email to '.$this->customer->user->email,
            'customer_name' => $this->customer->user->name,
            'purchased_items' => $this->formattedItems(),
            'payment' => $this->payments->sum('amount'),
            'payment_method' => ($this->payments->last() ? $this->payments->last()->payment_method : ""),
            'transaction' => $transaction,
        ];

        return $data;
    }

    private function formattedItems()
    {
        $formatted_items = [];
        foreach($this->items as $item)
        {
            array_push($formatted_items, [
                "name" => $item->product->name,
                "image" => $item->product->image,
                "brand" => $item->product->brand->name,
                "color" => $item->product->color->name,
                "size" => $item->product->size->name,
                "quantity" => $item->quantity,
                "price" => number_format($item->unit_price, 2),
                "total_amount" => number_format($item->quantity * $item->unit_price, 2),
            ]);
        }

        return $formatted_items;
    }
}
