<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Auth\StatefulGuard;
use Laravel\Fortify\Http\Responses\LogoutResponse as FortifyLogoutResponse;
use Auth;

class LogoutResponse extends FortifyLogoutResponse
{
    protected $guard;

    public function __construct(StatefulGuard $guard)
    {
        $this->guard = $guard;
    }

    public function toResponse($request)
    {
        auth('sanctum')->user()->tokens()->delete();
        
        return response([
            'message' => 'Successfully logged out!',
        ]);
    }
}