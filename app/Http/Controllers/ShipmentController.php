<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Shipment;
use App\Http\Requests\Transaction\StoreShipmentRequest;
use App\Http\Requests\Transaction\UpdateShipmentRequest;
use App\Http\Resources\ShipmentResource;

class ShipmentController extends ApiController
{
    public function index(Transaction $transaction)
    {
        return ShipmentResource::collection($this->paginate($transaction->shipments));
    }

    public function store(StoreShipmentRequest $request, Transaction $transaction)
    {
        return $transaction->shipments()->create($request->toArray());
    }

    public function update(Transaction $transaction, UpdateShipmentRequest $request, Shipment $shipment)
    {
        $shipment->fill($request->only([
            'customer_address_id',
            'shipment_date', 
            'shipment_received',
        ]));
        
        if ($shipment->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $shipment->save();

        return $this->successResponse($shipment);
    }
    
    public function destroy(Transaction $transaction, $id)
    {
        $shipment = Shipment::find($id);
        return $shipment->delete();
    }
}