<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Luigel\Paymongo\Facades\Paymongo;
use App\Models\Transaction;
use App\Models\PaymentSourceIntent;
use App\Jobs\PaymongoCallbackResponseJob;

class PaymongoCallbackController extends Controller
{
    function sourceChargeable(Request $request)
    {
        // creating payment...
        $source_id = $request['data']['attributes']['data']['id'];
        $amount = number_format($request['data']['attributes']['data']['attributes']['amount'] /100, 2);
        $currency = $request['data']['attributes']['data']['attributes']['currency'];
        $description = $request['data']['attributes']['data']['attributes']['description'];
        $statement_descriptor = $request['data']['attributes']['data']['attributes']['statement_descriptor'];

        $payment = Paymongo::payment()
            ->create([
                'amount' => $amount,
                'currency' => $currency,
                'description' => $description,
                'statement_descriptor' => $statement_descriptor,
                'source' => [
                    'id' => $source_id,
                    'type' => 'source'
                ]
            ]);

        return response()->json(['success' => 'success'], 200);

    }

    function paymentPaid(Request $request)
    {
        $source_id = $request['data']['attributes']['data']['attributes']['source']['id'];
        $payment_intent_id = $request['data']['attributes']['data']['attributes']['payment_intent_id'];
        $payment_source_intent = $this->paymentSourceIntent($payment_intent_id, $source_id);
        
        // record payment
        $transaction = Transaction::find($payment_source_intent->transaction_id);

        if ($transaction) {
            $transaction->recordPayment( $this->formatPayment($request) );
            $transaction->sendConfirmationEmail();
        }

        return response()->json(['success' => 'success'], 200);
    }

    function formatPayment($request)
    {
        $payment = new \stdClass();
        $payment->id = $request['data']['attributes']['data']['id'];
        $payment->amount = number_format($request['data']['attributes']['data']['attributes']['amount'] /100, 2);
        $payment->method = $request['data']['attributes']['data']['attributes']['source']['type'];

        return $payment;
    }

    function paymentSourceIntent($payment_intent_id, $source_id)
    {
        return PaymentSourceIntent::where('payment_intent_id', $payment_intent_id)
                                    ->orWhere('source_id', $source_id)->first();
    }

    function paymentFailed(Request $request)
    {
        return "Payment Failed";
    }
}
