<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use Elasticsearch\Client;
use App\Http\Requests\Transaction\StoreTransactionRequest;
use App\Http\Requests\Transaction\UpdateTransactionRequest;
use App\Http\Resources\TransactionResource;
use PDF;

class TransactionController extends ApiController
{
    public function index()
    {
        $collection = Transaction::all();
        return TransactionResource::collection($this->paginate($collection));
    }

    public function store(StoreTransactionRequest $request)
    {      
        $transaction =  Transaction::create([
            'invoice_no' => $request->input('invoice_no'),
            'customer_id' => $request->input('customer_id'),
            'total_amount' => 0,
            'balance' => 0,
        ]);

        foreach($request->items AS $key=>$item)
        {
            $transaction->items()->create([
                'product_id' => $item["product_id"],
                'unit_price' => $item["unit_price"],
                'quantity' => $item["quantity"]
            ]);
        }
        
        return $this->successResponse($transaction, 201);
    }

    public function show(Transaction $transaction)
    {
        return $transaction->payment_source_intents;
    }

    public function update(UpdateTransactionRequest $request, Transaction $transaction)
    {
        $transaction->fill($request->only([
            'invoice_no',
            'invoice_date',
            'customer_id',
        ]));
        
        if ($transaction->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $transaction->save();

        return $this->successResponse($transaction);
    }
    
    public function destroy(Transaction $transaction)
    {
        foreach ($transaction->items as $item){
            $item->delete();
        }
        $transaction->delete();
        return $this->successResponse($transaction);
    }

    public function search($to_search, Client $client)
    {
        $repository = new TransactionRepository($client);
        $repository->searchParams($to_search);
        return $repository->collection();
    }

    public function search_by_date($from, $to, Client $client)
    {
        $repository = new TransactionRepository($client);
        $repository->searchByDateParams(
            date("m/d/Y H:i", strtotime($from)), 
            date("m/d/Y H:i", strtotime($to))
        );
        return $repository->collection();
    }

    public function pdf(Transaction $transaction)
    {
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('pdf.confirmation', $transaction->dataset());
        return $pdf->stream();
    }
}
