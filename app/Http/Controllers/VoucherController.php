<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Voucher\StoreVoucherRequest;
use App\Http\Requests\Voucher\UpdateVoucherRequest;
use App\Http\Resources\VoucherResource;
use App\Models\Voucher;

class VoucherController extends ApiController
{
    public function index()
    {
        $collection = Voucher::all();
        return VoucherResource::collection($this->paginate($collection));
    }
    
    public function store(StoreVoucherRequest $request)
    {
        return Voucher::create($request->toArray());
    }

    public function show(Voucher $voucher)
    {
        return new VoucherResource($voucher);
    }

    public function update(UpdateVoucherRequest $request, Voucher $voucher)
    {   
        $voucher->fill($request->only([
            'code',
            'description',
            'discount_amount',
        ]));
        
        if ($voucher->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $voucher->save();

        return $this->successResponse($voucher, 200);
    }
    
    public function destroy(Voucher $voucher)
    {
        return $voucher->delete();   
    }
}
