<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Size;
use App\Http\Requests\StoreUpdateNameOnlyRequest;
use App\Http\Resources\SizeResource;

class SizeController extends ApiController
{
    public function index()
    {
        $collection = Size::all();
        return SizeResource::collection($this->paginate($collection));
    }
    
    public function store(StoreUpdateNameOnlyRequest $request)
    {
        return Size::create($request->toArray());
    }

    public function show(Size $size)
    {
        return $size;
    }

    public function update(StoreUpdateNameOnlyRequest $request, Size $size)
    {   
        $size->fill($request->only([
            'name',
        ]));
        
        if ($size->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $size->save();

        return $this->successResponse($size, 200);
    }
    
    public function destroy(Size $size)
    {
        return $size->delete();   
    }
}
