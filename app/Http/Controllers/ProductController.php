<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Repositories\ProductReviewRepository;
use Illuminate\Support\Arr;
use Elasticsearch\Client;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\ProductResource;

class ProductController extends ApiController
{
    public function index()
    {
        $collection = Product::all();
        return ProductResource::collection($this->paginate($collection));
    }

    public function store(StoreProductRequest $request)
    {
        return Product::create($request->toArray());
    }

    public function show(Product $product)
    {
        return $product;
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->fill($request->only([
            'stock_code',
            'name',
            'availability',
            'unit_price',
            'brand_id',
            'color_id',
            'size_id'
        ]));
        
        if ($product->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $product->save();

        return $this->successResponse($product, 200);
    }

    public function destroy(Product $product)
    {
        return $product->delete();
    }

    public function search(string $to_search, Client $client)
    {
        $repository = new ProductRepository($client);
        $repository->search($to_search);
        return $repository->collection();
    }

    public function search_by_price(float $from, float $to, Client $client)
    {
        $repository = new ProductRepository($client);
        $repository->searchByPriceRange($from, $to);
        return $repository->collection();
    }

    public function filter(Request $request, Client $client)
    {
        $repository = new ProductRepository($client);

        if ($request->has('category')) {
            $repository->searchByCategory($request->category);
        }
        if ($request->has('search')) {
            $repository->search($request->search);
        }
        if ($request->has('price_range')) {
            $repository->searchByPriceRange($request->price_range["from"], $request->price_range["to"]);
        }
        if ($request->has('brand')) {
            $repository->searchByBrand($request->brand);
        }
        if ($request->has('color')) {
            $repository->searchByColor($request->color);
        }
        if ($request->has('size')) {
            $repository->searchBySize($request->size);
        }
        return $repository->collection();
    }

    public function rating_percentage(Product $product, Client $client) 
    {
        $repository = new ProductReviewRepository($client);
        return $repository->ratingPercentage($product->stock_code);
    }
}
