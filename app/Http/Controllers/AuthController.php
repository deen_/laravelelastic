<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
    public function register(RegisterUserRequest $request)
    {
        $request["password"] = bcrypt($request->password);
        $user = User::create($request->toArray());

        $accessToken = $user->createToken('authToken')->accessToken;
        return response([
            "user" => $user,
            "access_token" => $accessToken
        ]);
    }

    public function login(LoginRequest $request)
    {
        if (!Auth::attempt($request->toArray())) {
            return response(['message' => 'Invalid credentials.']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response([
            "user" => auth()->user(),
            "access_token" => $accessToken
        ]);
    }
}
