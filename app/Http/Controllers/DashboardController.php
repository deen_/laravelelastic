<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TransactionRepository;
use App\Repositories\ProductReviewRepository;
use Elasticsearch\Client;

use App\Http\Requests\Dashboard;

class DashboardController extends Controller
{
    public function total_customers(Request $request, Client $client)
    {
        $repository = new TransactionRepository($client);
        return $repository->totalCustomers($request->count);
    }

    public function total_transactions(Dashboard\GetTotalTransactionRequest $request, Client $client)
    {
        $repository = new TransactionRepository($client);
        if ($request->within_date_range) {
            $repository->searchByDateParams(
                date("m/d/Y H:i", strtotime($request->from)), 
                date("m/d/Y H:i", strtotime($request->to))
            );
        }
        return $repository->totalTransactions($request->count);
    }
    
    public function revenue(Dashboard\GetRevenueRequest $request, Client $client)
    {
        $repository = new TransactionRepository($client);
        if ($request->within_date_range) {
            $repository->searchByDateParams(
                date("m/d/Y H:i", strtotime($request->from)), 
                date("m/d/Y H:i", strtotime($request->to))
            );
        }
        return $repository->revenue($request->type);
    }   

    public function top_sales(Dashboard\GetTopSalesRequest $request, Client $client)
    {
        $repository = new TransactionRepository($client);
        if ($request->within_date_range) {
            $repository->searchByDateParams(
                date("m/d/Y H:i", strtotime($request->from)), 
                date("m/d/Y H:i", strtotime($request->to))
            );
        }
        return $repository->topSales($request->count);
    }

    public function top_customers(Dashboard\GetTopCustomersRequest $request, Client $client)
    {
        $repository = new TransactionRepository($client);
        if ($request->within_date_range) {
            $repository->searchByDateParams(
                date("m/d/Y H:i", strtotime($request->from)), 
                date("m/d/Y H:i", strtotime($request->to))
            );
        }
        return $repository->topCustomers($request->count);
    }

    public function product_sales(Dashboard\GetProductSalesRequest $request, Client $client)
    {
        $repository = new TransactionRepository($client);
        if ($request->within_date_range) {
            $repository->searchByDateParams(
                date("m/d/Y H:i", strtotime($request->from)), 
                date("m/d/Y H:i", strtotime($request->to))
            );
        }
        return $repository->productSales($request->stock_code);
    }
    
    public function average_transaction_sale(Dashboard\GetAverageTransactionSaleRequest $request, Client $client)
    {
        $repository = new TransactionRepository($client);
        if ($request->within_date_range) {
            $repository->searchByDateParams(
                date("m/d/Y H:i", strtotime($request->from)), 
                date("m/d/Y H:i", strtotime($request->to))
            );
        }
        return $repository->averageTransactionSale($request->type);
    }  

    public function top_performing_category(Dashboard\GetTopPerformingCategoryRequest $request, Client $client)
    {        
        $repository = new TransactionRepository($client);
        if ($request->within_date_range) {
            $repository->searchByDateParams(
                date("m/d/Y H:i", strtotime($request->from)), 
                date("m/d/Y H:i", strtotime($request->to))
            );
        }
        return $repository->topPerformingCategory($request->count);
    }

    public function top_rated_per_category(Dashboard\GetTopRatedPerCategoryRequest $request, Client $client)
    {        
        $repository = new ProductReviewRepository($client);
        return $repository->topRatedPerCategory($request->count, $request->category);
    }
}
