<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Http\Requests\StoreUpdateProductImageRequest;
use App\Http\Resources\ProductImageResource;

class ProductImageController extends ApiController
{
    public function index(Product $product)
    {
        return ProductImageResource::collection($this->paginate($product->images));
    }
    
    public function store(StoreUpdateProductImageRequest $request, Product $product)
    {
        return $product->images()->create($request->toArray());
    }
    
    public function show(Product $product, $productImage)
    {
        return ProductImage::find($productImage);
    }

    public function update(Product $product, StoreUpdateProductImageRequest $request, $id)
    {
        $productImage = ProductImage::find($id);
        $productImage->fill($request->only([
            'product_id',
            'path',
        ]));
        
        if ($productImage->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $productImage->save();

        return $this->successResponse($productImage, 200);
    }

    public function destroy(Product $product, $id)
    {
        $productImage = ProductImage::find($id);
        return $productImage->delete();
    }
}
