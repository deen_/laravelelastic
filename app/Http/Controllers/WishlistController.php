<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Wishlist;
use App\Http\Resources\WishlistResource;

class WishlistController extends ApiController
{
    public function index(Customer $customer)
    {   
        return WishlistResource::collection($this->paginate($customer->wishlist));
    }

    public function store(Request $request, Customer $customer)
    {
        return $customer->wishlist()->create($request->toArray());
    }

    public function destroy(Customer $customer, $id)
    {
        $wishlist = Wishlist::find($id);
        return $wishlist->delete();
    }
}
