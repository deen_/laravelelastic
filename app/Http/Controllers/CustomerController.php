<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Customer;
use App\Http\Requests\Customer\StoreCustomerRequest;
use App\Http\Requests\Customer\UpdateCustomerRequest;
use App\Http\Resources\CustomerResource;

class CustomerController extends ApiController
{
    public function index()
    {
        $collection = Customer::all();
        return CustomerResource::collection($this->paginate($collection));
    }

    public function store(StoreCustomerRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);

        $customer = $user->customer()->create([
            'gender' => $request->gender,
            'dob' => $request->dob,
        ]);

        return new CustomerResource($customer);
    }

    public function show(Customer $customer)
    {
        return new CustomerResource($customer);
    }

    public function update(UpdateCustomerRequest $request, Customer $customer)
    {
        $customer->fill($request->only([
            'gender',
            'dob',
        ]));

        $customer->user->fill($request->only([
            'name',
            'password',
        ]));
        
        if ($customer->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $customer->save();

        return $this->successResponse($customer, 200);
    }

    public function destroy(Customer $customer)
    {
        return $customer->delete();
    }
}
