<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductReview;
use App\Http\Requests\ProductReview\StoreProductReviewRequest;
use App\Http\Requests\ProductReview\UpdateProductReviewRequest;
use App\Http\Resources\ProductReviewResource;

class ProductReviewController extends ApiController
{
    public function index(Product $product)
    {
        return ProductReviewResource::collection($this->paginate($product->reviews));
    }

    public function store(StoreProductReviewRequest $request, Product $product)
    {
        return $product->reviews()->create($request->toArray());
    }

    public function show(Product $product, $productReview)
    {
        return ProductReview::find($productReview);
    }

    public function update(Product $product, UpdateProductReviewRequest $request, $id)
    {
        $productReview = ProductReview::find($id);
        $productReview->fill($request->only([
            'product_id',
            'customer_id',
            'star',
            'review',
        ]));
        
        if ($productReview->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $productReview->save();

        return $this->successResponse($productReview, 200);
    }

    public function destroy(Product $product, $id)
    {
        $productReview = ProductReview::find($id);
        return $productReview->delete();
    }
}
