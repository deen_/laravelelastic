<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Http\Requests\Customer\StoreCustomerAddressRequest;
use App\Http\Requests\Customer\UpdateCustomerAddressRequest;
use App\Http\Resources\CustomerAddressResource;

class CustomerAddressController extends ApiController
{
    public function index(Customer $customer)
    {
        return CustomerAddressResource::collection($this->paginate($customer->addresses));
    }

    public function store(StoreCustomerAddressRequest $request, Customer $customer)
    {
        return $customer->addresses()->create($request->toArray());
    }

    public function update(Customer $customer, UpdateCustomerAddressRequest $request, CustomerAddress $address)
    {
        $address->fill($request->only([
            'full_name',
            'barangay_id', 
            'postal_code',
            'detailed_address',
            'default_address',
        ]));
        
        if ($address->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $address->save();

        return $this->successResponse($address);
    }

    public function destroy(Customer $customer, $id)
    {
        $address = CustomerAddress::find($id);
        return $address->delete();
    }
}