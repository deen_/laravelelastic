<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Cart;
use App\Http\Resources\CartResource;
use App\Http\Requests\Customer\StoreCartRequest;
use App\Http\Requests\Customer\UpdateCartRequest;

class CartController extends ApiController
{
    public function index(Customer $customer)
    {
        return CartResource::collection($this->paginate($customer->cart));
    }
    
    function store(Customer $customer, StoreCartRequest $request)
    {
        $cart = $customer->cart()->create($request->toArray());

        return new CartResource($cart);
    }

    public function show(Customer $customer, Cart $cart)
    {
        return new CartResource($cart);
    }

    public function update(Customer $customer, UpdateCartRequest $request, Cart $cart)
    {
        $cart->fill($request->only([
            'quantity',
        ]));
        
        if ($cart->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $cart->save();

        return $this->successResponse($cart, 200);
    }

    public function destroy(Customer $customer, Cart $cart)
    {
        return $cart->delete();   
    }
}
