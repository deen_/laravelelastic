<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Http\Requests\StoreUpdateNameOnlyRequest;
use App\Http\Resources\BrandResource;

class BrandController extends ApiController
{
    public function index()
    {
        $collection = Brand::all();
        return BrandResource::collection($this->paginate($collection));
    }
    
    public function store(StoreUpdateNameOnlyRequest $request)
    {
        return Brand::create($request->toArray());
    }

    public function show(Brand $brand)
    {
        return $brand;
    }

    public function update(StoreUpdateNameOnlyRequest $request, Brand $brand)
    {   
        $brand->fill($request->only([
            'name',
        ]));
        
        if ($brand->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $brand->save();

        return $this->successResponse($brand, 200);
    }
    
    public function destroy(Brand $brand)
    {
        return $brand->delete();   
    }
}
