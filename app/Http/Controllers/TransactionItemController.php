<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\TransactionItem;
use App\Http\Requests\TransactionItem\StoreTransactionItemRequest;
use App\Http\Requests\TransactionItem\UpdateTransactionItemRequest;
use App\Http\Resources\TransactionItemResource;

class TransactionItemController extends ApiController   
{
    public function index(Transaction $transaction)
    {
        return TransactionItemResource::collection($this->paginate($transaction->items));
    }

    public function store(StoreTransactionItemRequest $request, Transaction $transaction)
    {
        return $transaction->items()->create($request->toArray());
    }

    public function show($transaction, $transaction_item)
    {
        return TransactionItem::find($transaction_item);
    }

    public function update(Transaction $transaction, UpdateTransactionItemRequest $request, $item)
    {
        $transaction_item = TransactionItem::find($item);
        $transaction_item->fill($request->only([
            'product_id',
            'unit_price',
            'quantity',
        ]));
        
        if ($transaction_item->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $transaction_item->save();

        return $this->successResponse($transaction_item, 200);
    }

    public function destroy(Transaction $transaction, $item)
    {
        $transaction_item = TransactionItem::find($item);
        return $transaction_item->delete();
    }
}