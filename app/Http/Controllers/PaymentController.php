<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Luigel\Paymongo\Facades\Paymongo;
use App\Http\Requests\Payment\PaymentIntentRequest;
use App\Http\Requests\Payment\PaymentMethodRequest;
use App\Http\Requests\Payment\StorePaymentRequest;
use App\Http\Requests\Payment\AuthorizePaymentRequest;
use App\Models\Transaction;
use App\Models\Payment;

class PaymentController extends Controller
{
    function index(Transaction $transaction)
    {
        return $transaction->payments;
    }

    // Payment Intent workflow
    function payment_intent(Transaction $transaction, PaymentIntentRequest $request)
    {
        $paymentIntent = Paymongo::paymentIntent()->create([
            'amount' => $request->amount,
            'payment_method_allowed' => [
                'paymaya', 'card'
            ],
            'payment_method_options' => [
                'card' => [
                    'request_three_d_secure' => 'automatic'
                ]
            ],
            'description' => $this->formatDescription($request, $transaction),
            'statement_descriptor' => "Invoice No.: ".$transaction->invoice_no,
            'currency' => $request->currency,
        ]);

        // print_r($paymentIntent);
        $transaction->payment_source_intents()->create([
            "source_id" => "",
            "payment_intent_id" => $paymentIntent->id
        ]);

        return [
            "paymentIntentID" => $paymentIntent->id
        ];
    }
    
    function payment_method(Transaction $transaction, PaymentMethodRequest $request)
    {
        $address = $request->billing["address"];
        $data = [
            'type' => $request->type,
            'billing' => [
                'address' => [
                    'line1' => $address["line1"],
                    'city' => $address["city"],
                    'state' => $address["state"],
                    'country' => $address["country"],
                    'postal_code' => $address["postal_code"],
                ],
                'name' => $request->billing["name"],
                'email' => $request->billing["email"],
                'phone' => $request->billing["phone"],
            ],
        ];

        if ($request->type == "card") {
            $data['details'] = [
                'card_number' => $request->details["card_number"],
                'exp_month' => $request->details["exp_month"],
                'exp_year' => $request->details["exp_year"],
                'cvc' => $request->details["cvc"]
            ];
        }

        $paymentMethod = Paymongo::paymentMethod()
            ->create($data);
        $paymentIntent = Paymongo::paymentIntent()->find($request->paymentIntentID);

        $attachedPaymentIntent = $paymentIntent->attach($paymentMethod->id, env("PAYMENT_SUCCESS_REDIRECT"));

        $result =  ["status" => $attachedPaymentIntent->status];

        switch ($attachedPaymentIntent->status) {

            case "awaiting_next_action" :
                $result["next_action"] = [
                    "redirect" => $attachedPaymentIntent->next_action["redirect"]["url"]
                ];
                break;
            case "succeeded":
                $payment = new \stdClass();
                $payment->id = $attachedPaymentIntent->payments[0]['id'];
                $payment->amount = number_format($attachedPaymentIntent->payments[0]['attributes']['amount'] /100, 2);
                $payment->method = $request->type;
                
                $transaction->recordPayment($payment);
                $transaction->sendConfirmationEmail();
                break;
            case "awaiting_payment_method":
                // The PaymentIntent encountered a processing error. You can refer to paymentIntent.attributes.last_payment_error to check the error and render the appropriate error message.
                break;
            case "processing":
                // You need to requery the PaymentIntent after a second or two. This is a transitory status and should resolve to `succeeded` or `awaiting_payment_method` quickly.
                break;
        } 
        return $result;
    }
    // ./Payment Intent workflow

    function authorize_payment(Transaction $transaction, AuthorizePaymentRequest $request)
    {
        $source = Paymongo::source()->create([
            'type' => $request->type, // gcash or grab_pay
            'amount' => $request->amount,
            'currency' => $request->currency,
            'description' => $this->formatDescription($request, $transaction),
            'statement_descriptor' => "Invoice No.: ".$transaction->invoice_no,
            'transaction_id' => $transaction->id,
            'redirect' => [
                'success' => env("PAYMENT_SUCCESS_REDIRECT"),
                'failed' => env("PAYMENT_FAILED_REDIRECT")
            ]
        ]);

        // save source_id to transaction
        $transaction->payment_source_intents()->create([
            "source_id" => $source->id,
            "payment_intent_id" => ""
        ]);

        // print_r($source);
        return [
            "sourceID" => $source->id,
            "redirect" => $source->redirect['checkout_url']
        ];
    }

    function formatDescription($request, $transaction) 
    {
        if ($request->amount == $transaction->total_amount) {
            $description = "Full Payment";
        } else {
            $description = "Partial Payment";
        }
        
        $description .= " | Invoice No.: ".$transaction->invoice_no;
        return $description;
    }
    
    function success_payment()
    {
        return "Payment Successful!";
    }

    function failed_payment()
    {
        return "Payment Failed!";
    }
}