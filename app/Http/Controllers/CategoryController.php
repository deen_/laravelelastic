<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\StoreUpdateNameOnlyRequest;
use App\Http\Resources\CategoryResource;

class CategoryController extends ApiController
{
    public function index()
    {
        $collection = Category::all();
        return CategoryResource::collection($this->paginate($collection));
    }
    
    public function store(StoreUpdateNameOnlyRequest $request)
    {
        return Category::create($request->toArray());
    }

    public function show(Category $category)
    {
        return $category;
    }

    public function update(Request $request, Category $category)
    {   
        $category->fill($request->only([
            'name',
            'parent_category_id'
        ]));
        
        if ($category->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $category->save();

        return $this->successResponse($category, 200);
    }
    
    public function destroy(Category $category)
    {
        return $category->delete();   
    }
}
