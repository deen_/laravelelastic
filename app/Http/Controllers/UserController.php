<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserResource;

class UserController extends ApiController
{
    public function index()
    {
        $collection = User::all();
        return UserResource::collection($this->paginate($collection));
    }

    function show(User $user)
    {
        return $user;
    }
    
    public function update(Request $request, User $user)
    {
        $user->fill($request->only([
            'email',
            'password',
        ]));
        
        if ($user->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $user->save();

        return $this->successResponse($user, 200);
    }

    public function destroy(User $user)
    {
        return $user->delete();  
    }
}
