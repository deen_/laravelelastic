<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Color;
use App\Http\Requests\StoreUpdateNameOnlyRequest;
use App\Http\Resources\ColorResource;

class ColorController extends ApiController
{
    public function index()
    {
        $collection = Color::all();
        return ColorResource::collection($this->paginate($collection));
    }
    
    public function store(StoreUpdateNameOnlyRequest $request)
    {
        return Color::create($request->toArray());
    }

    public function show(Color $color)
    {
        return $color;
    }

    public function update(StoreUpdateNameOnlyRequest $request, Color $color)
    {   
        $color->fill($request->only([
            'name',
        ]));
        
        if ($color->isClean()) {
            return $this->errorResponse('You need to specify any different value to update', 422);
        }
        $color->save();

        return $this->successResponse($color, 200);
    }
    
    public function destroy(Color $color)
    {
        return $color->delete();   
    }
}
