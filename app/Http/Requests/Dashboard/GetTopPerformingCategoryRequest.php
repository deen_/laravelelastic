<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class GetTopPerformingCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count' => 'required',
            'type' => 'required',
            'within_date_range' => 'required|boolean',
            'from' => 'required_if:within_date_range,true|date',
            'to' => 'required_if:within_date_range,true|date'
        ];
    }
}
