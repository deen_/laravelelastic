<?php

namespace App\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;

class PaymentMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paymentIntentID' => 'required',
            'type' => 'required',
            'billing.address.line1' => 'required',
            'billing.address.city' => 'required',
            'billing.address.state' => 'required',
            'billing.address.country' => 'required',
            'billing.address.postal_code' => 'required',
            'billing.name' => 'required',
            'billing.email' => 'required',
            'billing.phone' => 'required',
            'details.card_number' => 'required_if:type,"card"',
            'details.exp_month' => 'required_if:type,"card"',
            'details.exp_year' => 'required_if:type,"card"',
            'details.cvc' => 'required_if:type,"card"'
        ];
    }
}
