<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'invoice_no' => $this->transaction->invoice_no,
            'invoice_date' => $this->transaction->invoice_date,
            'customer' => ($this->transaction->customer ? $this->transaction->customer->name : ""),
            'product' => [
                'stock_code' => $this->product->stock_code,
                'name' => $this->product->name,
                'price' => $this->unit_price,
                'quantity' => $this->quantity,
            ]
        ];
    }
}
