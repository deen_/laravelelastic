<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'stock_code' => $this->stock_code,
            'name' => $this->name,
            'image' => $this->image,
            'category' => ($this->category ? $this->category->name : ""),
            'brand' => ($this->brand ? $this->brand->name : ""),
            'color' => ($this->color ? $this->color->name : ""),
            'size' => ($this->size ? $this->size->name : ""),
            'availability' => $this->availability,
            'unit_price' => $this->unit_price
        ];
    }
}
