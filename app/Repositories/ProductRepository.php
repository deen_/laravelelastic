<?php

namespace App\Repositories;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository extends ElasticRepository
{
    protected $resource = ProductResource::class;
    protected $model = Product::class;

    /** @var \Elasticsearch\Client */
    
    public function __construct($client)
    {
        $this->client = $client;
        $this->params = [
            'index' => 'products',
            'type' => '_doc',
            'body' => [
                'query' => new class{}
            ]
        ];
        $this->params['body']['query']->bool = new class{};
        $this->params['body']['query']->bool->must = [];
    }

    public function search(string $query = '')
    {
        $this->params['body']['query']->bool->must[] = [
            "multi_match" => [
                "query" =>   $query,
                "fields" => ["name^2", "stock_code", "category", "price"],
                "fuzziness" => "AUTO",
            ]
        ];
    }
    
    public function searchByPriceRange($from, $to)
    {
        $this->params['body']['query']->bool->filter = [
            "range" => [
                "unit_price" => [
                    "gte" => $from,
                    "lte" => $to
                ]
            ]
        ];
    }

    public function searchByCategory($category)
    {
        $this->params['body']['query']->bool->must[] = [
            "match" => [
                "category" => $category
            ]
        ];
    }

    public function searchByBrand($brand)
    {
        $this->params['body']['query']->bool->must[] = [
            "match" => [
                "brand" => $brand
            ]
        ];
    }

    public function searchByColor($color)
    {
        $this->params['body']['query']->bool->must[] = [
            "match" => [
                "color" => $color
            ]
        ];
    }

    public function searchBySize($size)
    {
        $this->params['body']['query']->bool->must[] = [
            "match" => [
                "size" => $size
            ]
        ];
    }
}