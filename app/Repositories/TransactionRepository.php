<?php

namespace App\Repositories;

use App\Http\Resources\TransactionItemResource;
use App\Models\TransactionItem;
use App\Models\Product;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;

class TransactionRepository extends ElasticRepository
{
    /** @var \Elasticsearch\Client */

    protected $intervals;
    protected $resource = TransactionItemResource::class;
    protected $model = TransactionItem::class;
    
    public function __construct($client)
    {
        $this->client = $client;
        $this->intervals = [
            "daily" => "day",
            "monthly" => "month",
            "yearly" => "year"
        ];
        $this->params = [
            'index' => 'transactions',
            'type' => '_doc',
            'body' => []
        ];
    }

    public function searchParams(string $query = '')
    {
        $this->paginate();
        $this->params['body']['query'] = [
            "multi_match" => [
                "query" =>   $query,
                "fields" => ["invoice_no", "stock_code", "description"],
                "type" => "phrase"
            ]
        ];
    }

    public function searchByDateParams(string $from = '', string $to = '')
    {
        $this->paginate();
        $this->params['body']['query'] = [
            "range" => [
                "invoice_date" => [
                  "gte" => $from,
                  "lte" => $to
                ]
            ]
        ];
    }

    private function totalCustomersParams()
    {
        $this->params['body']['size'] = 0;
        $this->params['body']['aggs'] = [
            "total_customers" => [    
                "cardinality" => [
                    "field" => "customer_id"
                ]
            ]
        ];
    }

    private function totalTransactionsParams()
    {
        $this->params['body']['size'] = 0;
        $this->params['body']['aggs'] = [
            "total_transactions" => [    
                "cardinality" => [
                    "field" => "invoice_no.keyword"
                ]
            ]
        ];
    }

    private function revenueParams($type)
    {
        $this->paginate();
        $this->params['body']['size'] = 0;
        $this->params['body']['aggs'] = [
            "transactions_per_".$this->intervals[$type] => [
                "date_histogram" => [
                    "field" => "invoice_date",
                    "calendar_interval" => $this->intervals[$type]
                ],
                "aggs" => [
                    $type."_revenue" => [
                        "sum" => [
                            "script" => [
                                "source" => "doc['unit_price'].value * doc['quantity'].value"
                            ]   
                        ]
                    ],
                    "number_of_unique_customers" => [
                        "cardinality" => [
                            "field" => "customer_id"
                        ]
                    ]
                ]
            ]
        ];
    }

    private function topSalesParams($count)
    {
        $this->paginate();
        $this->params['body']['size'] = 0;
        $this->params['body']['aggs'] = [
            "top_sales" => [
                "terms" => [
                    "field" => "stock_code.keyword",
                    "order" => ["sales_volume" => "desc"],
                    "size" => $count
                ],
                "aggs" => [
                    "sales_volume" => [
                        "sum" => [
                            "field" => "quantity"
                        ]
                    ],
                    "transaction" => [
                        "top_hits" => [
                            "size" => 1
                        ]
                    ],
                ]
            ]
        ];
    }

    private function topCustomersParams($count)
    {
        $this->paginate();
        $this->params['body']['size'] = 0;
        $this->params['body']['aggs'] = [
            "top_customers" => [
                "terms" => [
                    "field" => "customer_id",
                    "order" => ["total_sales" => "desc"],
                    "size" => $count
                ],
                "aggs" => [
                    "total_sales" => [
                        "sum" => [
                            "script" => [
                                "source" => "doc['unit_price'].value * doc['quantity'].value"
                            ]
                        ]
                    ],
                    "transaction" => [
                        "top_hits" => [
                            "size" => 1
                        ]
                    ],
                ]
            ]
        ];
    }

    private function productSalesParams($stock_code)
    {
        $this->params['body']['query'] = [
            "bool" => [
                "must" => [
                    "match_phrase" => [
                        "stock_code" => $stock_code
                    ]
                ]
            ]
        ];
        $this->params['body']['aggs']['total_volume'] = [
            "sum" => [
                "field" => "quantity"
            ]
        ];
        $this->params['body']['aggs']['total_sales'] = [
            "sum" => [
                "script" => [
                    "source" => "doc['unit_price'].value * doc['quantity'].value"
                ]
            ]
        ];
    }

    private function averageTransactionSaleParams($type)
    {
        $this->paginate();
        $this->params['body']['size'] = 0;
        $this->params['body']['aggs'] = [
            "transactions_per_".$this->intervals[$type] => [
                "date_histogram" => [
                    "field" => "invoice_date",
                    "calendar_interval" => $this->intervals[$type]
                ],
                "aggs" => [
                    "total_sales" => [
                        "sum" => [
                            "script" => [
                                "source" => "doc['unit_price'].value * doc['quantity'].value"
                            ]   
                        ]
                    ],
                    "total_transactions" => [
                        "cardinality" => [
                            "field" => "invoice_no.keyword"
                        ]
                    ],
                    $type."_average_trx_sale" => [
                        "bucket_script" => [
                            "buckets_path" => [
                              "my_var1" => "total_sales",
                              "my_var2" => "total_transactions"
                            ],
                            "script" => "params.my_var1 / params.my_var2"
                        ]
                    ]
                ]
            ]
        ];
    }

    private function topPerformingCategoryParams($count)
    {
        $this->paginate();
        $this->params['body']['size'] = 0;
        $this->params['body']['aggs'] = [
            "top_performing_category" => [
                "terms" => [
                    "field" => "category.keyword",
                    "size" => $count
                ]
            ]
        ];
    }

    # --------------------------------------

    public function totalCustomers()
    {
        $this->totalCustomersParams();
        $items = $this->elasticsearch();

        return [
            "total_customers" => $items["aggregations"]["total_customers"]["value"]
        ];
    }

    public function totalTransactions()
    {
        $this->totalTransactionsParams();
        $items = $this->elasticsearch();

        return [
            "total_transactions" => $items["aggregations"]["total_transactions"]["value"]
        ];
    }

    public function revenue($type)
    {
        $this->revenueParams($type);
        $items = $this->elasticsearch();
        $buckets = $items['aggregations']['transactions_per_'.$this->intervals[$type]]['buckets'];
        $total_count = $items['hits']['total']['value'];

        $collection = collect($buckets)->map(function($row) use ($type) {
            return [
                "date" => $row["key_as_string"],
                $type."_revenue" => $row[$type."_revenue"]["value"]
            ];
        });

        return $this->paginateCollection($collection, $total_count);
    }

    public function topSales($count)
    {
        $this->topSalesParams($count);
        $items = $this->elasticsearch();
        $buckets = $items['aggregations']['top_sales']['buckets'];

        $collection =  collect($buckets)->map(function($row){
            return [
                "stock_code" => $row["key"],
                "product" => $row["transaction"]["hits"]["hits"][0]["_source"]["description"],
                "sales_volume" => $row["sales_volume"]["value"]
            ];
        });
        
        return $this->paginateCollection($collection, $count);
    }

    public function topCustomers($count)
    {
        $this->topCustomersParams($count);
        $items = $this->elasticsearch();
        $buckets = $items['aggregations']['top_customers']['buckets'];

        $collection =  collect($buckets)->map(function($row){
            return [
                "customer" => $row["transaction"]["hits"]["hits"][0]["_source"]["customer"],
                "total_sales" => $row["total_sales"]["value"],
                "total_transactions" => $row["transaction"]["hits"]["total"]["value"]
            ];
        });
        
        return $this->paginateCollection($collection, $count);
    }

    public function productSales($stock_code)
    {
        $this->productSalesParams($stock_code);
        $items = $this->elasticsearch();
        $ids = Arr::pluck($items['hits']['hits'], '_id');
        
        return [
            "total_transactions" => $items['hits']['total']['value'],
            "total_volume" => $items['aggregations']['total_volume']['value'],
            "total_sales" => $items['aggregations']['total_sales']['value']
        ];
    }

    public function averageTransactionSale($type)
    {
        $this->averageTransactionSaleParams($type);
        $items = $this->elasticsearch();
        $buckets = $items['aggregations']['transactions_per_'.$this->intervals[$type]]['buckets'];
        $total_count = $items['hits']['total']['value'];

        $collection = collect($buckets)->map(function($row) use ($type) {
            return [
                "date" => $row["key_as_string"],
                "total_transactions" => $row["total_transactions"]["value"],
                "total_sales" => $row["total_sales"]["value"],
                $type."_average_transaction_sale" => $row[$type."_average_trx_sale"]["value"]
            ];
        });

        return $this->paginateCollection($collection, $total_count);
    }

    public function topPerformingCategory($count)
    {
        $this->topPerformingCategoryParams($count);
        $items = $this->elasticsearch();
        $buckets = $items['aggregations']['top_performing_category']['buckets'];

        $collection =  collect($buckets)->map(function($row){
            return [
                "category" => $row["key"],
                "transactions" => $row["doc_count"]
            ];
        });
        
        return $this->paginateCollection($collection, $count);
    }
}