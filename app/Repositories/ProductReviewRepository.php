<?php

namespace App\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;

class ProductReviewRepository extends ElasticRepository
{
    protected $resource = ProductResource::class;
    protected $model = Product::class;

    /** @var \Elasticsearch\Client */
    
    public function __construct($client)
    {
        $this->client = $client;
        $this->params = [
            'index' => 'product_reviews',
            'type' => '_doc',
            'body' => [
                'query' => new \stdClass()
            ]
        ];
    }

    public function ratingPercentageParams($stock_code)
    {
        $this->params['body']['size'] = 0;
        $this->params["body"]["query"] = [
            "match" => [
                "stock_code" => [
                    "query" =>   $stock_code
                ]
            ]
        ];

        $this->params["body"]["aggs"] = [
            "stars" => [
                "terms" => [
                    "field" => "star"
                ]
            ]
        ];
    }

    public function topRatedPerCategoryParams($count, $category)
    {
        $this->params['index'] = "product_reviews,transactions"; // search from multiple data stream
        // $this->params["body"]["size"] = 0;
        $this->params["body"]["query"] = [
            "match" => [
                "category" => [
                    "query" =>   $category
                ]
            ]
        ];

        $this->params["body"]["aggs"] = [
            "top_rated" => [
                "terms" => [
                    "field" => "stock_code.keyword",
                    "order" => ["stars" => "desc"],
                    // "size" => $count
                ],
                "aggs" => [
                    "stars" => [
                        "sum" => [
                            "field" => "star"
                        ]
                    ],
                    "transaction" => [
                        "top_hits" => [
                            "size" => 1
                        ]
                    ],
                ]
            ]
        ];
    }

    public function ratingPercentage($stock_code)
    {
        $this->ratingPercentageParams($stock_code);
        $items = $this->elasticsearch();
        $buckets = $items['aggregations']['stars']['buckets'];
        $total_count = $items['hits']['total']['value'];

        $collection = collect($buckets)->map(function($row) use ($total_count) {
            return [
                "star" => $row["key"],
                "count" => $row["doc_count"],
                "percentage" => ($row["doc_count"] * 100) / $total_count
            ];
        });

        return $collection->sortByDesc("star");
    }

    public function topRatedPerCategory($count, $category)
    {
        $this->topRatedPerCategoryParams($count, $category);
        $items = $this->elasticsearch();
        $buckets = $items['aggregations']['top_rated']['buckets'];

        $collection =  collect($buckets)->map(function($row){
            return [
                "product" => $row["transaction"]["hits"]["hits"][0]["_source"]["product_name"],
                "stars" => $row["stars"]["value"]
            ];
        });
        
        return $this->paginateCollection($collection, $count);
    }
}