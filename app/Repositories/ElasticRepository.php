<?php

namespace App\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ElasticRepository 
{
    protected $client, $params, $hits_from = 1, $hits_size = 5;

    public function elasticsearch()
    {
        // return $this->params;
        try {
            return $this->client->search($this->params);
            
        } catch (Missing404Exception $exception) {
            return [];
        }
    }

    public function paginate() 
    {
        $this->hits_from = LengthAwarePaginator::resolveCurrentPage();;
        $this->hits_size = 10;

        if (request()->has('per_page')) {
            $this->hits_size  = (int) request()->per_page;
        }

        $this->params['body'] = [
            "from" => ($this->hits_from-1) * $this->hits_size,
            "size" => $this->hits_size
        ];
    }

    public function collection()
    {
        $items = $this->elasticsearch();
        // return $items;
        
        $ids = Arr::pluck($items['hits']['hits'], '_id');
        $total_count = $items['hits']['total']['value'];

        $collection = $this->resource::collection($this->model::whereIn('id', $ids)->get());

        return $this->paginateCollection($collection, $total_count);
    }

    public function paginateCollection($collection, $total_count) 
    {
        $paginated = new LengthAwarePaginator($collection, $total_count, $this->hits_size, $this->hits_from, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

        $paginated->appends(request()->all());

        return $paginated;
    }
}