<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransactionConfirmationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = env('MAIL_FROM_ADDRESS');
        $name = env('MAIL_FROM_NAME');
        $subject = 'Transaction Confirmation | '.env('APP_NAME');

        return $this->view('emails.confirmation')
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([
                        'test_message' => $this->data['message'],
                        'customer_name' => $this->data['customer_name'],
                        'purchased_items' => $this->data['purchased_items'],
                        'payment' => $this->data['payment'],
                        'payment_method' => $this->data['payment_method'],
                        'transaction' => $this->data['transaction'],
                    ]);
    }
}
