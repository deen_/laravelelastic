<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Zen+Maru+Gothic:wght@300&display=swap" rel="stylesheet">
    
<link href="https://fonts.googleapis.com/css2?family=Fruktur&family=Zen+Maru+Gothic:wght@300&display=swap" rel="stylesheet">
    <style>
      
      html {
        font-family: 'Zen Maru Gothic', sans-serif;
        font-weight: normal;
        color: #0a2031;
        font-size: 1em;
        margin: 0;
      }

      body {
        padding: 80px 0 0 0;
        background-color: rgb(219, 248, 247);
        font-family: 'Zen Maru Gothic', sans-serif;
        font-weight: normal;
      }

      h1, h2 {
        font-family:sans-serif;
      }

      .wrapper {
        background-color: rgb(219, 248, 247);
        padding: 90px 0 0 0;
      }

      #watermark { 
        position: fixed; 
        bottom: 0px; 
        right: 0px; 
        width: 100%; 
        height: 100%; 
        z-index: -1;
        background-color: rgb(219, 248, 247);
      }

      .logo {
        position:fixed;
        top: -8px;
        left: 33%;
        z-index: -1;
      }

      h4 {
        margin-top: -7em;
      }

      .header {
        background-color: #154367;
        padding: 2em 1em;
        color: white;
      }

      hr {
        border: 1px solid #154367;
      }

      .body {
        padding: 0 3em;
        margin-top: 10px;
      }

      .center {
        text-align: center;
      }

      .right {
        text-align: right;
      }
      
      table, th, td {
        padding: 3px;
      }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      .item {
        width: 100%;
        height: auto;
        margin: 30px 0;
        display: block;
        overflow:overlay; 
      }

      .product-img {
        width: 45%;
        height: 250px;
        background-color: #7cb4c8;
        background-position: center;
        background-size: contain;
        background-repeat: no-repeat;
        border-radius: 10px;
      }

      .product-desc {
        position: absolute;
        top: 0;
        width: 50%;
        margin-left: 10px;
        float: right;
      }

      ul.leaders {
        width: 100%;
        padding: 0;
        margin: 0;
        overflow-x: hidden;
        list-style: none;
      }

      ul.leaders span {
        display: inline-block;
        width: 47%;

      }

      ul.leaders span + span {
        display: inline-block;
        width: 50%;
        text-align: right;
        margin-right: 10px;
      }

    </style>
  </head>
  <body>
    <div id="watermark"></div>

    <div class="wrapper">
      <img src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('logo.png'))) }}" class="logo">
      
      @include('_transaction')
    </div>
    <br><br><br>
    <hr>
    <div class="center">
      Thank you for shopping with us! :)
    </div>
  </body>
</html>