<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Zen+Maru+Gothic:wght@300&display=swap" rel="stylesheet">
    <style>

      html {
        font-family: 'Zen Maru Gothic', sans-serif;
        background-color: #d5e4e4;
        color: #0a2031;
        font-size: 1.2em;
      }

      .wrapper {
        margin: 5em;
        background-color: rgb(219, 248, 247);
        border-radius: 3em;
        padding-bottom: 3em;
      }

      .logo {
        display: block;
        margin-left: auto;
        margin-right: auto;
      }

      h4 {
        margin-top: -7em;
      }

      .header {
        background-color: #154367;
        padding: 1em;
        color: white;
      }

      hr {
        border: 1px solid #154367;
      }

      .body {
        padding: 0 5em;
      }

      .center {
        text-align: center;
      }

      .right {
        text-align: right;
      }
      
      table, th, td {
        padding: 3px;
      }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      .item {
        width: 100%;
        height: 20vw;
        display: flex;
        margin-bottom: 3em;
      }

      .product-img {
        width: 50%;
        height: 100%;
        background-color: #7cb4c8;
        background-position: center;
        background-size: contain;
        background-repeat: no-repeat;
      }

      .product-desc {
        width: 50%;
        margin-left: 3em;
      }

    ul.leaders {
      width: 100%;
      padding: 0;
      overflow-x: hidden;
      list-style: none
    }

    ul.leaders li:before {
      float: left;
      width: 0;
      white-space: nowrap;
      content:
        ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
        ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
        ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
    }
    ul.leaders span:first-child {
      padding-right: 0.33em;
      background: rgb(219, 248, 247)
    }
    ul.leaders span + span {
      float: right;
      padding-left: 0.33em;
      background: rgb(219, 248, 247)
    }

    .download {
      width: 30%;
      display: block;
      margin: auto;
      background-color: #154367;
      color: white;
      text-decoration: none;
      padding: 1em;
      text-align: center;
    }

    </style>
  </head>
  <body>
    <div class="wrapper">

      <img src="{{asset('logo.png')}}" alt="{{config('app.name')}}" class="logo">
      @include('_transaction')

      <a href="{{ route('transaction_pdf', ['transaction' => $transaction['id']])  }}" target="_blank" class="download">Download PDF</a>
      <br>
      <hr>
      <p class="center">Happy shopping! :)</p>
    </div>
  </body>
</html>