<div class="header center">
    <h1>Thank you for your order, {{ $customer_name }}!</h1>
    <p>You can find your purchase information below.</p>
</div>

<div class="body">
    <p><strong>PURCHASED ITEM(S):</strong></p>
    <div class="items">
    @foreach ($purchased_items as $item)
        <div class="item">
            <div class="product-img" style="background-image:url( {{ $item['image'] }} )"></div>
            <div class="product-desc">
                <table>
                <tr>
                    <td colspan="2">
                    <strong>{{ $item['name'] }}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Quantity:</td>
                    <td class="right">{{ $item['quantity'] }}</td>
                </tr>
                <tr>
                    <td>Unit Price:</td>
                    <td class="right">{{ $item['price'] }}</td>
                </tr>
                <tr>
                    <td>Amount:</td>
                    <td class="right">{{ $item['total_amount'] }}</td>
                </tr>
                <tr><td colspan="2"><hr></td></tr>
                <tr>
                    <td>Brand:</td>
                    <td class="right">{{ $item['brand'] }}</td>
                </tr>
                <tr>
                    <td>Color:</td>
                    <td class="right">{{ $item['color'] }}</td>
                </tr>
                <tr>
                    <td>Size:</td>
                    <td class="right">{{ $item['size'] }}</td>
                </tr>
                </table>
            </div>
        </div>
    @endforeach
    </div>

    <p><strong>ORDER TOTAL:</strong></p>
    <ul class=leaders>
    <li>
        <span>Subtotal</span>
        <span>{{ $transaction['sub_total'] }}</span>
    </li>
    <li>
        <span>Shipping Fee</span>
        <span>{{ $transaction['shipping_amount'] }}</span>
    </li>
    <li>
        <span>Discount (less)</span>
        <span>{{ $transaction['discount_amount'] }}</span>
    </li>
    <li style="color:red; font-size: larger;" >
        <span>Total Amount</span>
        <span>{{ $transaction['total_amount'] }}</span>
    </li>
    </ul>
    <br>

    <p><strong>BILLING:</strong></p>
    <p>Payment Method: {{ $payment_method }}</p>
    <p>Payment: <strong>{{ $payment }}</strong> <i>{{ $transaction['payment_status'] }}</i></p>
    <p>Balance: <strong>{{ $transaction['balance'] }}</strong></p>
</div>