<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Color;
use App\Models\Size;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Transaction;
use App\Models\TransactionItem;
use App\Models\ProductReview;
use App\Models\Shipment;
use App\Models\Barangay;

use App\Jobs\IndexTransactionItemElasticsearchJob;

class DatabaseSeeder extends Seeder
{
    protected $data, $faker, $product, $transaction, $address;
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeder::class,
            BrandSeeder::class,
            ColorSeeder::class,
            SizeSeeder::class,
            BarangaySeeder::class,
        ]);

        Product::truncate();
        User::truncate();
        Customer::truncate();
        Transaction::truncate();
        TransactionItem::truncate();

        $this->faker = \Faker\Factory::create();
        $this->categories = Category::all()->pluck('id')->toArray();
        $this->brands = Brand::all()->pluck('id')->toArray();
        $this->colors = Color::all()->pluck('id')->toArray();
        $this->sizes = Size::all()->pluck('id')->toArray();
        $this->barangays = Barangay::all()->pluck('id')->toArray();
  
        $csvFile = fopen(base_path("database/data/data.csv"), "r");
  
        $firstline = true;
        $counter = 0;

        // for blank customer
        $this->createCustomer(0);

        while (($this->data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                
                $this->product = Product::where('stock_code', strtoupper($this->data['1']))->first();
                if ($this->product === null) 
                {
                    $this->createProduct();  
                }

                if ($this->data['6'] != "" AND $this->data['6'] != null AND Customer::find($this->data['6']) === null) 
                {
                    $this->createCustomer($this->data['6']);  
                }

                $this->transaction = Transaction::where('invoice_no', $this->data['0'])->first();
                if ($this->transaction === null) 
                {
                    $this->createTransaction();  
                    $this->createShipment();
                }  
                
                $this->createTransactionItem();  
                $this->createProductReview();  
                
                print_r("--".$counter++);
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }

    function createProduct() 
    {
        $this->product = Product::create([
            "stock_code" => strtoupper($this->data['1']),
            "name" => $this->data['2'],
            "image" => $this->faker->imageUrl($width = 200, $height = 100),
            "unit_price" => $this->data['5'],
            "category_id" => $this->faker->randomElement($this->categories),
            "brand_id" => $this->faker->randomElement($this->brands),
            "color_id" => $this->faker->randomElement($this->colors),
            "size_id" => $this->faker->randomElement($this->sizes),
        ]);    
    }

    function createCustomer($id) 
    {
        $gender = $this->faker->randomElement(['male', 'female']);

        $user = User::create([
            "name" => $this->faker->name($gender),
            "email" => $this->faker->email(),
            "password" => "password",
        ]);

        $user->customer()->create([
            "id" => $id,
            "gender" => $gender,
            'dob' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
        ]);
        
        $this->address = CustomerAddress::create([
            "customer_id" => $id,
            "full_name" => $this->faker->name,
            "barangay_id" => $this->faker->randomElement($this->barangays),
            "postal_code" => $this->faker->randomDigit,
            "detailed_address" => "",
            "default_address" => true
        ]);
    }

    function createTransaction()
    {
        $this->transaction = Transaction::create([
            "invoice_no" => $this->data['0'],
            "invoice_date" => $this->data['4'],
            "total_amount" => 0,
            "balance" => 0,
            "customer_id" => ($this->data['6'] == "" ? 0 : $this->data['6'])
        ]);  
    }

    function createTransactionItem()
    {
        $transaction_item = TransactionItem::create([
            "transaction_id" => $this->transaction->id,
            "product_id" => $this->product->id,
            "unit_price" => $this->data['5'],
            "quantity" => $this->data['3']
        ]);  

        $amt = $this->transaction->total_amount +  ($this->data['5'] * $this->data['3']);
        $this->transaction->sub_total = $amt;
        $this->transaction->total_amount = $amt;
        $this->transaction->balance = $amt;
        $this->transaction->save();
    }

    function createProductReview()
    {
        ProductReview::create([
            'star' => $this->faker->numberBetween(1,5),
            'review' => $this->faker->sentence(),
            'customer_id' => ($this->data['6'] == "" ? 0 : $this->data['6']),
            'product_id' => $this->product->id,
        ]);
    }

    function createShipment()
    {
        Shipment::create([
            'transaction_id' => $this->transaction->id,
            'customer_address_id' => $this->address->id,
            'shipment_date' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'shipment_received' => true
        ]);
    }
}
