<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Region;
use App\Models\Province;
use App\Models\City;
use App\Models\Barangay;

class BarangaySeeder extends Seeder
{
    protected $data, $city;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->region = Region::create(["name" => "Region V"]);
        $this->province = Province::create(["name" => "Albay", "region_id" => $this->region->id]);

        $csvFile = fopen(base_path("database/data/barangay.csv"), "r");
  
        $firstline = true;
        $counter = 0;

        while (($this->data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                $this->city = City::where('name', $this->data['1'])->first();
                if ($this->city === null) 
                {
                    $this->createCity();  
                }

                $this->createBarangay();
            }
            $firstline = false;
        }
    }

    protected function createCity()
    {
        $this->city = City::create([
            "name" => $this->data['1'],
            "province_id" => $this->province->id
        ]);
    }

    protected function createBarangay()
    {
        $this->barangay = Barangay::create([
            "name" => $this->data['0'],
            "city_id" => $this->city->id
        ]);
    }
}
