<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Size;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Size::insert([
            ["name" => "x-small"],
            ["name" => "small"],
            ["name" => "medium"],
            ["name" => "large"],
            ["name" => "x-large"]
        ]);   
    }
}
