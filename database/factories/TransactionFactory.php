<?php

namespace Database\Factories;

use App\Models\Transaction;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "invoice_no" => $this->faker->unique()->randomDigit,
            'customer_id' => function() {
                return Customer::factory()->create()->id;
            },
            'total_amount' => 100,
            'balance' => 100,
        ];
    }
}
