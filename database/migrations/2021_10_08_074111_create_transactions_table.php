<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_no')->unique();
            $table->dateTime('invoice_date')->default(Carbon::now());
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('voucher_id')->nullable();
            $table->decimal('sub_total', $precision = 8, $scale = 2)->default(0);
            $table->decimal('shipping_amount', $precision = 8, $scale = 2)->default(0);
            $table->decimal('discount_amount', $precision = 8, $scale = 2)->default(0);
            $table->decimal('total_amount', $precision = 8, $scale = 2)->default(0);
            $table->decimal('balance', $precision = 8, $scale = 2);
            $table->string('payment_status')->default('UNPAID');

            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('voucher_id')->references('id')->on('vouchers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
