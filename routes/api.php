<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TwoFactorAuthController;

use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductImageController;
use App\Http\Controllers\ProductReviewController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CustomerAddressController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\TransactionItemController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ShipmentController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PaymongoCallbackController;


use Laravel\Fortify\Features;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('users', UserController::class, ['except' => ['create', 'edit']]);
Route::resource('categories', CategoryController::class, ['except' => ['create', 'edit']]);
Route::resource('brands', BrandController::class, ['except' => ['create', 'edit']]);
Route::resource('colors', ColorController::class, ['except' => ['create', 'edit']]);
Route::resource('sizes', SizeController::class, ['except' => ['create', 'edit']]);
Route::resource('vouchers', VoucherController::class, ['except' => ['create', 'edit']]);

Route::resource('customers', CustomerController::class, ['except' => ['create', 'edit']]);
Route::resource('customers.wishlist', WishlistController::class, ['only' => ['index', 'store', 'destroy']]);
Route::resource('customers.addresses', CustomerAddressController::class, ['except' => ['create', 'edit']]);
Route::resource('customers.cart', CartController::class, ['except' => ['create', 'edit']]);

Route::resource('products', ProductController::class, ['except' => ['create', 'edit']]);
Route::resource('products.images', ProductImageController::class, ['except' => ['create', 'edit']]);
Route::resource('products.reviews', ProductReviewController::class, ['except' => ['create', 'edit']]);
Route::get('products/search/{search}', [ProductController::class, 'search']); 
Route::get('products/search_by_price/{from}/{to}', [ProductController::class, 'search_by_price']); 
Route::post('products/filter', [ProductController::class, 'filter']);
Route::get('products/{product}/rating_percentage', [ProductController::class, 'rating_percentage']);

Route::resource('transactions', TransactionController::class, ['except' => ['create', 'edit']]);
Route::resource('transactions.items', TransactionItemController::class, ['except' => ['create', 'edit']]);
Route::get('transactions/search/{search}', [TransactionController::class, 'search']); // invoice no, stock code, description
Route::get('transactions/search_by_date/{from}/{to}', [TransactionController::class, 'search_by_date']); // invoice date
Route::get('transactions/{transaction}/pdf', [TransactionController::class, 'pdf'])->name('transaction_pdf');
Route::resource('transactions.shipments', ShipmentController::class, ['except' => ['create', 'edit']]);
Route::resource('transactions.payments', PaymentController::class, ['only' => ['index']]);

Route::post('transactions/{transaction}/payment_intent', [PaymentController::class, 'payment_intent']);
Route::post('transactions/{transaction}/payment_method', [PaymentController::class, 'payment_method']);
Route::post('transactions/{transaction}/authorize_payment', [PaymentController::class, 'authorize_payment']);

Route::get('success_payment', [PaymentController::class, 'success_payment']);
Route::get('success_payment/{transaction}/save', [PaymentController::class, 'success_payment_save']);
Route::get('failed_payment', [PaymentController::class, 'failed_payment']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/2fa-confirm', [TwoFactorAuthController::class, 'confirm'])->name('two-factor.confirm');
    
    Route::get('total_customers', [DashboardController::class, 'total_customers']);
    Route::post('total_transactions', [DashboardController::class, 'total_transactions']);
    Route::post('revenue', [DashboardController::class, 'revenue']); // daily, monthly, yearly --within date range
    Route::post('top_sales', [DashboardController::class, 'top_sales']); // --within date range
    Route::post('top_customers', [DashboardController::class, 'top_customers']); // --within date range
    Route::post('product_sales', [DashboardController::class, 'product_sales']); // --within date range
    Route::post('average_transaction_sale', [DashboardController::class, 'average_transaction_sale']); // daily, monthly, yearly --within date range
    Route::post('top_performing_category', [DashboardController::class, 'top_performing_category']); // daily, monthly, yearly --within date range
    Route::post('top_rated_per_category', [DashboardController::class, 'top_rated_per_category']); // daily, monthly, yearly --within date range
});

Route::group(
    [
        'namespace' => 'Paymongo',
        'as' => 'paymongo.',
    ],
    function () {

        Route::post('/source-chargeable', [PaymongoCallbackController::class, 'sourceChargeable'])
            ->middleware('paymongo.signature:source_chargeable')
            ->name('source-chargeable');

        Route::post('/payment-paid', [PaymongoCallbackController::class, 'paymentPaid'])
            ->middleware('paymongo.signature:payment_paid')
            ->name('payment-paid');

        Route::post('/payment-failed', [PaymongoCallbackController::class, 'paymentFailed'])
            ->middleware('paymongo.signature:payment_failed')
            ->name('payment-failed');
    }
);

Route::post('mails', [PaymentController::class, 'sendEmail']);

