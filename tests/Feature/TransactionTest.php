<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Bus;

use App\Jobs\IndexTransactionItemElasticsearchJob;
use App\Jobs\UpdateTransactionElasticsearchJob;
use App\Jobs\UpdateTransactionItemElasticsearchJob;
use App\Jobs\RemoveTransactionItemElasticsearchJob;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\Customer;
use Illuminate\Testing\Fluent\AssertableJson;

class TransactionTest extends TestCase
{
    use RefreshDatabase;

    public function test_transactions_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        Transaction::factory()->count(5)->create();
        $transaction = Transaction::latest()->first();

        $response = $this->get('/api/transactions');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('invoice_no', $transaction->invoice_no)
                            ->where('customer_id', $transaction->customer_id)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_transaction_can_be_created()
    {
        $this->withoutExceptionHandling();

        Bus::fake();
        $customer = Customer::factory()->create();
        $product = Product::factory()->create();

        $response = $this->post('/api/transactions', [
            "invoice_no" => "DMF-010",
            "customer_id" => $customer->id,
            "total_amount" => 2,
            "balance" => 2,
            "items" => [
                [
                    "product_id" => $product->id,
                    "unit_price" => 2,
                    "quantity" => 1
                ]
            ]
        ]);

        Bus::assertDispatched(IndexTransactionItemElasticsearchJob::class);

        $response->assertStatus(201);
        $this->assertEquals(1, Transaction::count());
    }

    public function test_transaction_can_be_updated()
    {
        Bus::fake();
        
        $transaction = Transaction::factory()->create();
        $customer = Customer::factory()->create();
        
        $response = $this->put('/api/transactions/'.$transaction->id, [
            "customer_id" => $customer->id
        ]);

        Bus::assertDispatched(UpdateTransactionElasticsearchJob::class);

        $response->assertStatus(200);
    }

    public function test_transaction_can_be_deleted()
    {
        Bus::fake();
        $transaction = Transaction::factory()->hasItems()->create();

        $response = $this->delete('/api/transactions/'.$transaction->id);

        Bus::assertDispatched(RemoveTransactionItemElasticsearchJob::class);
        $response->assertStatus(200);

        $this->assertEquals(0, Transaction::count());
    }
}
