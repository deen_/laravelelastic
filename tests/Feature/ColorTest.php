<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Color;
use Illuminate\Testing\Fluent\AssertableJson;

class ColorTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_colors_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        Color::factory()->count(5)->create();
        $color = Color::latest()->first();

        $response = $this->get('/api/colors');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('id', $color->id)
                            ->where('name', $color->name)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_color_can_be_created()
    {
        $response = $this->post('/api/colors', [
            "name" => "Brown"
        ]);

        $response->assertStatus(201);
        $this->assertEquals(1, Color::count());
    }

    public function test_color_can_be_updated()
    {   
        $color = Color::factory()->create();
        
        $response = $this->put('/api/colors/'.$color->id, [
            "name" => "Pink"
        ]);

        $response->assertStatus(200);
    }

    public function test_color_can_be_deleted()
    {
        $color = Color::factory()->create();

        $response = $this->delete('/api/colors/'.$color->id);

        $response->assertStatus(200);
        $this->assertEquals(0, Color::count());
    }
}
