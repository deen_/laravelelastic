<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use App\Models\Product;
use App\Models\ProductReview;
use App\Models\Customer;
use Illuminate\Testing\Fluent\AssertableJson;

class ProductReviewTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_product_reviews_can_be_retrieved()
    {
        Bus::fake();
        $product = Product::factory()->hasReviews(5)->create();
        $productReview = $product->reviews->first();

        $response = $this->get('/api/products/'.$product->id.'/reviews');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('id', $productReview->id)
                            ->where('review', $productReview->review)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_product_review_can_be_created()
    {
        Bus::fake();
        $product = Product::factory()->create();
        $customer = Customer::factory()->create();

        $response = $this->post('/api/products/'.$product->id.'/reviews', [
            "star" => "5",
            "review" => "Nice product",
            "customer_id" => $customer->id,
        ]);

        $response->assertStatus(201);
        $this->assertEquals(1, ProductReview::count());
    }

    public function test_product_review_can_be_updated()
    {   
        Bus::fake();
        $this->withoutExceptionHandling();
        $product_review = ProductReview::factory()->create();
        
        $response = $this->put('/api/products/'.$product_review->product->id.'/reviews/'.$product_review->id, [
            "review" => "Updated review"
        ]);

        $response->assertStatus(200);
    }

    public function test_product_review_can_be_deleted()
    {
        Bus::fake();
        $product_review = ProductReview::factory()->create();

        $response = $this->delete('/api/products/'.$product_review->product->id.'/reviews/'.$product_review->id);

        $response->assertStatus(200);
        $this->assertEquals(0, ProductReview::count());
    }
}
