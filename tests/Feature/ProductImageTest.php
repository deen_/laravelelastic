<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Testing\Fluent\AssertableJson;

class ProductImageTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_product_images_can_be_retrieved()
    {
        Bus::fake();
        
        $product = Product::factory()->hasImages(5)->create();
        $productImage = $product->images->first();

        $response = $this->get('/api/products/'.$product->id.'/images');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('id', $productImage->id)
                            ->where('path', $productImage->path)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_product_image_can_be_created()
    {
        Bus::fake();
        $product = Product::factory()->create();
        $response = $this->post('/api/products/'.$product->id.'/images', [
            "path" => "some-path"
        ]);

        $response->assertStatus(201);
        $this->assertEquals(1, ProductImage::count());
    }

    public function test_product_image_can_be_updated()
    {   
        Bus::fake();
        $this->withoutExceptionHandling();
        $product_image = ProductImage::factory()->create();
        
        $response = $this->put('/api/products/'.$product_image->product->id.'/images/'.$product_image->id, [
            "path" => "updated-path"
        ]);

        $response->assertStatus(200);
    }

    public function test_product_image_can_be_deleted()
    {
        Bus::fake();
        $product_image = ProductImage::factory()->create();

        $response = $this->delete('/api/products/'.$product_image->product->id.'/images/'.$product_image->id);

        $response->assertStatus(200);
        $this->assertEquals(0, ProductImage::count());
    }
}
