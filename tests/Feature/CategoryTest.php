<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Category;
use Illuminate\Testing\Fluent\AssertableJson;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_categories_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        Category::factory()->count(5)->create();
        $category = Category::latest()->first();

        $response = $this->get('/api/categories');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('id', $category->id)
                            ->where('name', $category->name)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_category_can_be_created()
    {
        $parent_category = Category::factory()->create();

        $response = $this->post('/api/categories', [
            "name" => "Category A",
            "parent_category_id" => $parent_category->id
        ]);

        $response->assertStatus(201);
        $this->assertEquals(2, Category::count());
    }

    public function test_category_can_be_updated()
    {   
        $category = Category::factory()->create();
        
        $response = $this->put('/api/categories/'.$category->id, [
            "name" => "Category B"
        ]);

        $response->assertStatus(200);
    }

    public function test_category_can_be_deleted()
    {
        $category = Category::factory()->create();

        $response = $this->delete('/api/categories/'.$category->id);

        $response->assertStatus(200);
        $this->assertEquals(0, Category::count());
    }
}
