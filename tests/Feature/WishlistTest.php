<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use App\Models\Wishlist;
use App\Models\Customer;
use App\Models\Product;
use Illuminate\Testing\Fluent\AssertableJson;

class WishlistTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_wishlists_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        Bus::fake();
        $customer = Customer::factory()->hasWishlist(5)->create();

        $wishlist = $customer->wishlist->first();
        $response = $this->get('/api/customers/'.$customer->id.'/wishlist');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('product_id', $wishlist->product_id)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_wishlist_can_be_created()
    {
        Bus::fake();

        $customer = Customer::factory()->hasUser()->create();
        $product = Product::factory()->create();

        $response = $this->post('/api/customers/'.$customer->id.'/wishlist', [
            "product_id" => $product->id
        ]);

        $response->assertStatus(201);
        $this->assertEquals(1, Wishlist::count());
    }


    public function test_wishlist_can_be_deleted()
    {
        Bus::fake();
        $wishlist = Wishlist::factory()->create();

        $response = $this->delete('/api/customers/'.$wishlist->customer->id.'/wishlist/'.$wishlist->id);

        $response->assertStatus(200);
        $this->assertEquals(0, Wishlist::count());
    }
}
