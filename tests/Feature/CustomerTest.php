<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;

use App\Models\User;
use App\Models\Customer;

class CustomerTest extends TestCase
{
    use RefreshDatabase;

    public function test_customers_can_be_retrieved()
    {
        $user = User::factory()->create();
        $customer = Customer::factory()->for($user)->create();

        $response = $this->get('/api/customers');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 1, fn ($json) =>
                        $json->where('id', $customer->id)
                            ->where('name', $customer->user->name)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_customer_can_be_created()
    {
        $response = $this->post('/api/customers', [
            "name" => "John Doe",
            "email" => "john@example.com",
            "password" => "password",
            "password_confirmation" => "password"
        ]);

        $response->assertStatus(201);
        $this->assertEquals(1, Customer::count());
    }

    public function test_customer_can_be_updated()
    {   
        $user = User::factory()->create();
        $customer = Customer::factory()->for($user)->create();
        
        $response = $this->put('/api/customers/'.$customer->id, [
            "dob" => "2002-03-17"
        ]);

        $response->assertStatus(200);
    }

    public function test_customer_can_be_deleted()
    {
        $customer = Customer::factory()->create();

        $response = $this->delete('/api/customers/'.$customer->id);

        $response->assertStatus(200);
        $this->assertEquals(0, Customer::count());
    }
}
