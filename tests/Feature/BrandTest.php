<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Brand;
use Illuminate\Testing\Fluent\AssertableJson;

class BrandTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_brands_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        Brand::factory()->count(5)->create();
        $brand = Brand::latest()->first();

        $response = $this->get('/api/brands');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('id', $brand->id)
                            ->where('name', $brand->name)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_brand_can_be_created()
    {
        $response = $this->post('/api/brands', [
            "name" => "Brand A"
        ]);

        $response->assertStatus(201);
        $this->assertEquals(1, Brand::count());
    }

    public function test_brand_can_be_updated()
    {   
        $brand = Brand::factory()->create();
        
        $response = $this->put('/api/brands/'.$brand->id, [
            "name" => "Brand B"
        ]);

        $response->assertStatus(200);
    }

    public function test_brand_can_be_deleted()
    {
        $brand = Brand::factory()->create();

        $response = $this->delete('/api/brands/'.$brand->id);

        $response->assertStatus(200);
        $this->assertEquals(0, Brand::count());
    }
}
