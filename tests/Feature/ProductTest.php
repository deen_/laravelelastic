<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Testing\Fluent\AssertableJson;

use App\Jobs\IndexProductElasticsearchJob;
use App\Jobs\UpdateProductElasticsearchJob;
use App\Jobs\RemoveProductElasticsearchJob;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Color;
use App\Models\Size;
use App\Models\Category;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function test_products_can_be_retrieved()
    {
        Bus::fake();
        Product::factory()->count(5)->create();
        $product = Product::latest()->first();

        $response = $this->get('/api/products');
        
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('stock_code', $product->stock_code)
                            ->where('name', $product->name)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_product_can_be_created()
    {
        $this->withoutExceptionHandling();

        Bus::fake();

        $category = Category::factory()->create();
        $brand = Brand::factory()->create();
        $color = Color::factory()->create();
        $size = Size::factory()->create();

        $response = $this->post('/api/products', [
            "stock_code" => "STK-001",
            "name" => "PENCIL",
            "availability" => "5",
            "unit_price" => "2",
            "category_id" => $category->id,
            "brand_id" => $brand->id,
            "color_id" => $color->id,
            "size_id" => $size->id,
        ]);

        Bus::assertDispatched(IndexProductElasticsearchJob::class);

        $response->assertStatus(201);
        $this->assertEquals(1, Product::count());
    }

    public function test_product_can_be_updated()
    {
        Bus::fake();
        
        $product = Product::factory()->create();
        
        $response = $this->put('/api/products/'.$product->id, [
            "availability" => "3"
        ]);

        Bus::assertDispatched(UpdateProductElasticsearchJob::class);

        $response->assertStatus(200);
    }

    public function test_product_can_be_deleted()
    {
        Bus::fake();
        $product = Product::factory()->create();

        $response = $this->delete('/api/products/'.$product->id);

        Bus::assertDispatched(RemoveProductElasticsearchJob::class);
        $response->assertStatus(200);
        $this->assertEquals(0, Product::count());
    }
}
