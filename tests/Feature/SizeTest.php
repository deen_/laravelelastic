<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Size;
use Illuminate\Testing\Fluent\AssertableJson;

class SizeTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_sizes_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        Size::factory()->count(5)->create();
        $size = Size::latest()->first();

        $response = $this->get('/api/sizes');
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data', 5, fn ($json) =>
                        $json->where('id', $size->id)
                            ->where('name', $size->name)
                            ->etc()
                    )
                    ->has('links')
                    ->has('meta')
            );
        $response->assertStatus(200);
    }

    public function test_size_can_be_created()
    {
        $response = $this->post('/api/sizes', [
            "name" => "Small"
        ]);

        $response->assertStatus(201);
        $this->assertEquals(1, Size::count());
    }

    public function test_size_can_be_updated()
    {   
        $size = Size::factory()->create();
        
        $response = $this->put('/api/sizes/'.$size->id, [
            "name" => "Medium"
        ]);

        $response->assertStatus(200);
    }

    public function test_size_can_be_deleted()
    {
        $size = Size::factory()->create();

        $response = $this->delete('/api/sizes/'.$size->id);

        $response->assertStatus(200);
        $this->assertEquals(0, Size::count());
    }
}
